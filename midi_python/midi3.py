#!/usr/bin/env python

# http://www.nntp.perl.org/group/perl.midi/2004/10/msg283.html
# http://home.roadrunner.com/~jgglatt/tech/midifile.htm

import midi2, sys
import struct
import pygame

notelist = { }

def register_note(track, channel, pitch, value, time_down, time_up):
    if notelist.has_key(track) == False:
        notelist[track] = []
    notelist[track].append((channel, pitch, value, time_down, time_up))

midi2.register_note = register_note

m = midi2.MidiFile()
m.open(sys.argv[1])
m.read()

print "Available tracks:",
for track in notelist:
    print track,
print

pygame.mixer.pre_init(44100, -16, 2, 1024)
pygame.init()

pygame.mixer.music.load(sys.argv[2])

print "ticks per quarter note:", m.ticksPerQuarterNote

smtpe = 0.0

for e in m.tracks[0].events:
    if e.type == "SET_TEMPO":
        bytes = struct.unpack(">BBB", e.data)
        tempo = 0
        for b in bytes:
            tempo += b
            tempo <<= 8
        tempo >>= 8
        bpm = 60000000.0/tempo

    elif e.type == "TIME_SIGNATURE":
        bytes = struct.unpack(">BBBB", e.data)
        signature = str(bytes[0])+"/"+str(2**bytes[1])
        ticks_per_beat = bytes[3]

    elif e.type == "SMTPE_OFFSET":
        bytes = struct.unpack(">BBBBB", e.data)
        #print "SMTPE: 0x%x 0x%x 0x%x 0x%x 0x%x" % bytes
        #smtpe = (bytes[1] * 60.0 + bytes[2]) * 1000

print "Signature:", signature, "Tempo:", tempo, "BPM:", bpm, "SMTPE:", smtpe

tpm = tempo/m.ticksPerQuarterNote/1000.0#(ticks_per_beat*1000.0)/(tempo/m.ticksPerQuarterNote)

print "ticks_per_msec:", tpm

#sys.exit(0)


c = pygame.time.Clock()
c.tick()

pygame.mixer.music.play()

track = notelist[int(sys.argv[3])]
while len(track):
    note = track[0]
    track = track[1:]
    channel, pitch, value, time_down, time_up = note
    time = pygame.mixer.music.get_pos()
    while time_down*tpm > time:
        c.tick(40)
        time = pygame.mixer.music.get_pos()
        print #time, time_down
    print (time_down, channel, pitch, value) ,

    

