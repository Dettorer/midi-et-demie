#!/usr/bin/env python

# http://www.sonicspot.com/guide/midifiles.html

import sys
import os
from struct import unpack

tempo = 500000
timediv = 0

def print_bin(val):
    print "[",
    for i in range(8):
        print (val >> i) & 1,
    print "]",

def read_varl(fd):
    res = 0
    buff = fd.read(1)
    byte, = unpack(">B", buff)
    res = byte & 127
    n = 1
    #print_bin(byte)
    while byte & (1 << 7) != 0:
        buff = fd.read(1)
        byte, = unpack(">B", buff)
        res += byte & 127
        n += 1
        #print_bin(byte)
    return res, n

def read_metaevent(fd):
    global tempo
    type, = unpack(">B", fd.read(1))
    size, n = read_varl(fd)
    print "MetaEvent of type 0x%x(%i) and size %i:" %( type, type, size)
    if type == 6:
        label, = unpack(">"+str(size)+"s", fd.read(size))
        print "Marker:", label
    elif type == 3:
        label, = unpack(">"+str(size)+"s", fd.read(size))
        print "Track Name:", label
    elif type == 81:
        b1, b2, b3 = unpack(">BBB", fd.read(3))
        tempo = b3 + (b2 << 8) + (b1 << 16)
        print "Tempo (micro / quarter-note):", tempo
        print "Tempo (BPM):", 60000000.0 / tempo
    elif type == 84:
        bytes = unpack(">BBBBB", fd.read(5))
        print bytes
    else:
        #print
        if size != 0:
            fd.seek(size, os.SEEK_CUR)
    return n + size + 1

event_def = { # event_number : (event_name, param_nbr, (params_names)) 
    8: ("NoteOff", 2, ("NoteNbr", "Velocity")),
    9: ("NoteOn", 2, ("NoteNbr", "Velocity")),
    10: ("NoteAftertouch", 2, ("NoteNbr", "Amount")),
    11: ("ControlerEvent", 2, ("CtrlType", "Value") ),    
    12: ("ProgramChange", 1, ( "Program" ) ),
    13: ("ChannelAftertouch", 1, ("Amount") ),
    14: ("PitchBend", 2, ("Value[LSB]", "Value[MSB]") ),
}

def read_event(fd):
    deltatime, n = read_varl(fd)
    print deltatime * tempo / timediv,
    eventtype_midichannel, = unpack(">B", fd.read(1))
    #print_bin(eventtype_midichannel)
    n += 1
    if eventtype_midichannel == 0xFF:
        n += read_metaevent(fd)
    elif eventtype_midichannel == 0xF0 or eventtype_midichannel == 0xF7:
        res, size = read_varl(fd)
        print "SystemExclive Event of size",res,", skipping."
        fd.seek(res, os.SEEK_CUR)
        n = n + res + size
    else:
        eventtype = (eventtype_midichannel & 0xF0) >> 4
        midichannel = (eventtype_midichannel & 0x0F) 
        if event_def.has_key(eventtype):
            ename, pnbr, params = event_def[eventtype]
            if pnbr == 1:
                param1, = unpack(">B", fd.read(1))
                param1_name = params
                print "Event:", ename, "Channel:", midichannel, param1_name+":", param1
            else:
                param1, param2 = unpack(">BB", fd.read(2))
                param1_name, param2_name = params
                print "Event:", ename, "Channel:", midichannel, param1_name+":", param1, param2_name+":", param2
        else:
            print "Unknown event! Abord!", eventtype
            sys.exit(0)

        n += 2
    return n

def read_mtrk(fd, size):
    print "Read MTrk"
    n = size
    while size > 0:
        size -= read_event(fd)

def read_chunk(fd):
    buff = fd.read(8)
    if len(buff) == 8:
        id, size = unpack(">4sI", buff)
    else:
        id, size = ("", 0)
    return id, size

def parse_midi(fd):
    global timediv
    id, size = read_chunk(fd)
    while size != 0:
        if id == "MThd":
            type, ntracks, timediv = unpack(">HHH", fd.read(size))
            print "Midi Header:"
            print "  Type:",type
            print "  Number of tracks:", ntracks
            if timediv > 0:
                print "  Time division (PPQ):", timediv
            else :
                print "  Time division (SMPTE):", -timediv
        elif id == "MTrk":
            read_mtrk(fd, size)
        else:
            print "Unknown chunk type:", id
            fd.seek(size, os.SEEK_CUR)
        id, size = read_chunk(fd)
        

if __name__ == "__main__":
    fd = open(sys.argv[1])
    parse_midi(fd)
